<?php

declare(strict_types=1);

namespace Wx1860\WxCommon\Exception\Handler;

use Hyperf\ExceptionHandler\ExceptionHandler;
use Psr\Http\Message\ResponseInterface;

use Hyperf\Validation\ValidationException;
use Hyperf\HttpMessage\Exception\NotFoundHttpException;
use Hyperf\HttpMessage\Exception\MethodNotAllowedHttpException;
use Hyperf\HttpMessage\Exception\ServerErrorHttpException;
use Hyperf\HttpMessage\Exception\BadRequestHttpException;
use Hyperf\HttpMessage\Exception\ForbiddenHttpException;
use Throwable;
use Wx1860\WxCommon\Constants\Code;
use Wx1860\WxCommon\Utils\ResponseTrait;

class AppServiceExceptionHandler extends ExceptionHandler
{
    use ResponseTrait;

    public function __construct()
    {

    }

    public function handle(Throwable $throwable, ResponseInterface $response)
    {
        //阻止异常冒泡
        $this->stopPropagation();

        $code = 0;
        if ($throwable instanceof ServerErrorHttpException) {
            $code = Code::SYSTEM_ERROR;
        }
        $this->fail($throwable->getCode(),$code,Code::getMessage($code));
    }

    public function isValid(Throwable $throwable): bool
    {
        return true;
    }
}
